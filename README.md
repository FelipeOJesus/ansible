Para iniciar, basta utilizar o comando vagrant up
Para destruir bastar utilizar o comando vagrant destroy -y

Segue abaixo o comando para executar o playbook, mas não é necessário, pois ao subir a maquina ansible, ela já executa o comando: 
ansible-playbook -i /vagrant/ansible/hosts /vagrant/ansible/provision.yml 
